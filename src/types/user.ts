export interface UserCreate {
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}

export interface User {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}

export interface Result<E> {
  page: number;
	per_page: number;
	total: number;
	total_pages: number;
	data: [E]
}