import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { UserCreate } from './types/user';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('users')
  getUsers () {
    return this.appService.getUsers();
  }

  @Post('users')
  createUser(@Body() payload: UserCreate) {
    return this.appService.createUser(payload);
  }
}
