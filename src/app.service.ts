import { Injectable } from '@nestjs/common';
import axios from 'axios'
import { Result, User, UserCreate } from './types/user';

@Injectable()
export class AppService {
  async getUsers () {
    const users = await axios.get('https://reqres.in/api/users?page=2');
    console.log('USERS GET PAGE 2', users.data);
    return users.data as Result<User>;
  }

  async createUser (payload: UserCreate) {
    console.log('params', payload)
    const user = await axios.post('https://reqres.in/api/users', payload)
    console.log('CREATE USERS', user.data)
    return user.data as User
  }
}
